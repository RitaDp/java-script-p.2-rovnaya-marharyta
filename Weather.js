const axios = require('axios')

const weatherUrl = 'https://goweather.herokuapp.com/weather/'

class Weather{

    constructor() {
        this.temperature = ''
        this.wind = ''
        this.date = Date.now()
        this.forecast = []
        this.description = ''
    }

    async setWeather(cityName){
        await axios.get(weatherUrl + cityName).then((response) => {
            //console.log(response)
            this.temperature = response.data.temperature
            this.wind = response.data.wind
        }).catch((error) => {return error.message})
    }

    async setForecast (cityName){
        await axios.get(weatherUrl + cityName).then((response) => {
            this.forecast = JSON.stringify(response.data.forecast)
            this.description = response.data.description
        }).catch((error) => {return error.message})
    }
}

module.exports = {Weather}
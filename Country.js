const {City} = require('./City')

class Country {

    constructor(nameCountry) {
        this.nameCountry = nameCountry
        this.arrCity = []
    }

    addCity(name) {
        this.arrCity.push(new City(name))
    }

    delCityByName(name) {
        const index = this.arrCity.findIndex(n => n.name === name)
        //console.log(this.arrCity[index])
        this.arrCity.splice(index, 1)
    }

      sortCityByTemp (a, b) {
       // parseFloat(a.weather.temperature)
        //parseFloat(b.weather.temperature)
          if (parseFloat(a.weather.temperature) > parseFloat(b.weather.temperature)) {
            return -1
        }
          if (parseFloat(a.weather.temperature) < parseFloat(b.weather.temperature)) {
            return 1
        }
        return 0
    }

}

module.exports = {Country}